﻿using UnityEngine;
using System.Collections;

public class Nave : MonoBehaviour
{
    public Transform cannon;
    public GameObject shot;
    public GameObject explosion;
    SpriteRenderer spriteRenderer;
    bool shooting;
    Animator animator;

    RightJoystick joystick;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        joystick = FindObjectOfType<RightJoystick>();

        animator = GetComponentInChildren<Animator>();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        spriteRenderer.material.color = new Color(1, 1, 1, .5f);

        Physics2D.IgnoreLayerCollision(0, 8);
        yield return new WaitForSeconds(2);
        Physics2D.IgnoreLayerCollision(0, 8, false);

        spriteRenderer.material.color = new Color(1, 1, 1, 1);
    }

    // Update is called once per frame
    void Update()
    {
        float h = 0;
        float v = 0;

#if UNITY_EDITOR || UNITY_STANDALONE
        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");
#else
        if (joystick)
        {
            Vector3 direction = joystick.GetInputDirection();
            h = direction.x;
            v = direction.y;
        }
#endif
        Vector3 newPos = transform.position;
        newPos.x += h * Time.deltaTime * 5;
        newPos.y += v * Time.deltaTime * 5;
        transform.position = newPos;

        if (animator)
        {
            animator.SetFloat("Horizontal", h);
        }

#if UNITY_EDITOR || UNITY_STANDALONE
        if (Input.GetKey(KeyCode.Space))
        {
            if (!shooting)
                StartCoroutine(Shot());
        }
#else
        if (!shooting)
            StartCoroutine(Shot());
#endif
    }

    IEnumerator Shot()
    {
        shooting = true;

        GameObject bullet =
            Instantiate(shot, cannon.position, cannon.rotation);
        Destroy(bullet, 3);

        float time = 0.1f;
        Tiro tiro = bullet.GetComponent<Tiro>();
        if (tiro) time = tiro.rate;

        yield return new WaitForSeconds(time);

        shooting = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<Pedra>())
        {
            GameObject boom = Instantiate(explosion,
                                            transform.position,
                                            transform.rotation);

            FindObjectOfType<Spawn>().SpawnShip(1);
            Destroy(boom, 3);
            Destroy(gameObject);
        }
    }
}
