﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Spawn : MonoBehaviour
{
    public int vidas = 3;
    public GameObject nave;
    public GameObject gameOver;
    public GameObject highScoreTxt;
    public Button restart, mainMenu;
    public AudioSource audio;
    LifeCounter lifeCounter;

    public Button exit;
    public Button pause;
    bool paused;

    public GameObject joystick;
    private void Awake()
    {
        #if UNITY_EDITOR || UNITY_STANDALONE
        joystick.gameObject.SetActive(false);
        #else
        joystick.gameObject.SetActive(true);
        #endif
    }

    // Start is called before the first frame update
    void Start()
    {
        Physics2D.IgnoreLayerCollision(8, 9);
        restart.onClick.AddListener(Restart);
        mainMenu.onClick.AddListener(()=> SceneManager.LoadScene(0));
        lifeCounter = FindObjectOfType<LifeCounter>();
        SpawnShip(.5f);

        exit.onClick.AddListener(Application.Quit);

        pause.image.color = new Vector4(1, 1, 1, 0.3f);
        pause.onClick.AddListener(() => 
        {
            if (paused) 
            {
                paused = false; 
                Time.timeScale = 1;
                audio.gameObject.SetActive(true);
                pause.image.color = new Vector4(1, 1, 1, 0.5f);
            }

            else
            {
                paused = true;
                Time.timeScale = 0;
                audio.gameObject.SetActive(false);
                pause.image.color = new Vector4(1, 1, 1, 1);
            }
        });
    }

    public void SpawnShip (float time)
    {
        if (vidas > 0)
        {
            vidas--;
            lifeCounter.UpdateValue(vidas + 1);
            StartCoroutine(SpawnRoutine(time));
            gameOver.SetActive(false);
            pause.gameObject.SetActive(true);
        }

        // Game Over
        else
        {
            audio.Stop();
            bool record = FindObjectOfType<Score>().SaveScore();
            highScoreTxt.SetActive(record);
            lifeCounter.UpdateValue(0);
            gameOver.SetActive(true);
            pause.gameObject.SetActive(false);
        }
    }

    IEnumerator SpawnRoutine (float time)
    {
        yield return new WaitForSeconds(time);
        Instantiate(nave, transform.position, transform.rotation);
    }

    void Restart ()
    {
        SceneManager.LoadScene(1);
    }
}
