﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public Button button;

    // Start is called before the first frame update
    void Start()
    {
        button.gameObject.SetActive(false);
        button.onClick.AddListener(StartGame);
        StartCoroutine(Splash());
    }

    IEnumerator Splash()
    {
        yield return new WaitForSeconds(1.5f);
        button.gameObject.SetActive(true);
    }
    void StartGame ()
    {
        SceneManager.LoadScene(1);
    }
}
