﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pedreira : MonoBehaviour
{
    public GameObject[] pedras;
    Spawn spawn;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Pedrada());
        spawn = FindObjectOfType<Spawn>();
    }

    IEnumerator Pedrada ()
    {
        float time = Random.Range(0.2f, 2);
        yield return new WaitForSeconds(time);

        int index = Random.Range(0, pedras.Length - 0);
        Vector3 position = transform.position;
        position.x = Random.Range(-2.5f, 2.5f);
        
        if (spawn.gameOver.active == false) {
            GameObject pedra =
                Instantiate(
                    pedras[index],
                    position,
                    transform.rotation);

            Destroy(pedra, 20);
        }

        StartCoroutine(Pedrada());
    }
}
